﻿USE ciclistas;

/** Consulta 1 Nombre y edad de los ciclistas que 
  han ganado etapas **/


  SELECT DISTINCT
 c.nombre,c.edad 
  FROM etapa e JOIN ciclista c ON e.dorsal = c.dorsal;




/**consulta 2 Nombre y edad de los ciclistas que han ganado puertos **/

  SELECT DISTINCT
  c.nombre, c.edad 
  FROM puerto p JOIN ciclista c ON p.dorsal = c.dorsal;


  /** consulta .3 Nombre y edad de los ciclistas que han
    ganado etapas y puertos
**/
-- consulta sin optimizar

SELECT DISTINCT
c.nombre, c.edad FROM ciclista c
JOIN etapa e ON c.dorsal = e.dorsal
JOIN puerto p ON c.dorsal = p.dorsal;


/** CONSULTA 4 Listar el director de los equipos que tengan 
  ciclistas que hayan ganado alguna etapa **/

  SELECT DISTINCT e1.director 
  FROM etapa e
    JOIN ciclista c ON e.dorsal = c.dorsal
    JOIN equipo e1 ON c.nomequipo = e1.nomequipo;


  /** CONSULTA 5. Dorsal y nombre de los ciclistas que hayan 
  llevado algún maillot **/

SELECT DISTINCT 
  c.dorsal,
  c.nombre
  FROM ciclista c JOIN lleva l ON c.dorsal = l.dorsal;




/** CONSULTA 6 Dorsal y nombre de los ciclistas que hayan llevado el
  maillot amarillo **/

  SELECT c.dorsal, c.nombre
  FROM ciclista c
    JOIN lleva l ON c.dorsal = l.dorsal
    JOIN maillot m ON l.código = m.código
    WHERE m.color="amarillo";
   

  /** Consulta 7 Dorsal de los ciclistas que hayan llevado algún maillot
  y que han ganado etapas **/

    SELECT DISTINCT
    e.dorsal
    FROM lleva l JOIN etapa e ON l.dorsal = e.dorsal;




/** consulta 8 Indicar el numetapa y los km de las etapas que
  tengan puertos **/

SELECT DISTINCT e.numetapa, e.kms FROM puerto p
JOIN etapa e ON p.numetapa = e.numetapa;


/** Consulta 9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan
puertos **/
SELECT DISTINCT 
e.kms  FROM puerto p JOIN etapa e ON p.numetapa = e.numetapa
JOIN ciclista c ON e.dorsal = c.dorsal
WHERE c.nomequipo="Banesto";


/** consulta 10 Listar el número de ciclistas que hayan ganado
  alguna etapa con puerto **/

  SELECT COUNT(DISTINCT etapa.dorsal) AS nciclistas
  FROM puerto JOIN etapa USING(numetapa);



/** consulta 11 11 Indicar el nombre de los puertos que hayan
  sido ganados por ciclistas de Banesto**/

  SELECT P.nompuerto
  FROM puerto p JOIN ciclista c ON p.dorsal = c.dorsal
  WHERE c.nomequipo="Banesto";

